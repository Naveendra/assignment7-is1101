#include <stdio.h>
#include <string.h>

int main()
{
	char arr[100];
	int a,x,count=0;
	int len;

	printf("Enter your sentence : \n ");
	fgets(arr,100,stdin);

	printf("Enter a character to find the frequency of the character\n");
	scanf("%c", &a);

	len = strlen(arr);
	for(x=0; x<=len; x++)
		if(arr[x]==a)
		{
			count+=1;
		}
	printf("The frequency of %c is %d", a, count);
	return 0;
}
